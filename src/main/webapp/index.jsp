<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ejercicio Calc</title>

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" />
<script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script defer
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

	<main>
		<div class="container" style="background-color: #fafafa">
			<div class="row justify-content-center">
				<div class="col-md-6">
					<form action="CalcServlet" method="POST">
						<h4>Calculadora</h4>
						<div class="form-group">
							<label for="txtNum1">Numero 1</label> <input type="text"
								class="form-control" id="txtNum1" name="txtNum1" />
						</div>
						<div class="form-group">
							<label for="txtNum2">Numero 2</label> <input type="text"
								class="form-control" id="txtNum2" name="txtNum2" />
						</div>

						<div class="form-group">
							<label for="operacion">Example select</label> <select
								class="form-control" id="oprecion" name="cboOperacion">
								<option value="0" disabled selected>Seleccione</option>
								<option value="suma">Sumar</option>
								<option value="resta">Restar</option>
								<option value="multi">Multiplicar</option>
								<option value="div">Dividir</option>
							</select>
						</div>
						<div class="form-group">
							<label for="operacion">Example select</label> <select
								class="form-control" id="oprecion" name="cboNumeros">
								<option value="0" disabled selected>Seleccione</option>
								<c:forEach var="i" items="1,4,5,6,7,8,9">
									<option value="<c:out value="${i}"></c:out>">
										<c:out value="${i}"></c:out>
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="alert alert-light" role="alert">${msg}</div>

						<div class="form-group">
							<label for="resultado">Resultado</label> <input type="text"
								class="form-control" id="resultado" value="${result}" readonly />
						</div>

						<div class="form-group">
							<input type="submit" class="btn btn-info" id="calcular"
								value="Calcular" name="calc">
						</div>
					</form>
				</div>
			</div>
		</div>
	</main>


</body>
</html>